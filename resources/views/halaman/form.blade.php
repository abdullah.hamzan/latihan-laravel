@extends('layout.master')

@section('judul')

Form Registerasi

@endsection

@section('content')
<h2>Buat Account Baru</h2>
<h4>Sign Up Form</h4>

<form action="welcome" method="post">
    @csrf
    <p>First Name:</p>
    <input type="text" name="firstName"><br>
    <p>Last Name:</p>
    <input type="text" name="lastName"><br>
    <p>Gender:</p>
    <input type="radio" name="gender" value="male">Male <br>
    <input type="radio" name="gender" value="female">Female <br>
    <input type="radio" name="gender" value="other">Other <br>

    <p>Nationality:</p>
    <select name="" id="">
        <option value="1">Indonesia</option>
        <option value="2">Malaysia</option>
        <option value="1">Timur Leste</option>
    </select>
    <p>Language Spoken:</p>
    <input type="checkbox" name="bahasa" id="" value="indo"> Bahasa Indonesia <br>
    <input type="checkbox" name="bahasa" id="" value="english"> Englsih <br>
    <input type="checkbox" name="bahasa" id="" value="other"> Other <br>
    <p>Bio:</p>
    <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">


</form>

@endsection


