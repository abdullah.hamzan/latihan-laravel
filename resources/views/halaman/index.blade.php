
@extends('layout.master')


@section('judul')

List Film

@endsection


@section('content')


<div class="row">
    @foreach ($films as $item)
    <div class="col-4">


<div class="card" style="width: 18rem;">
    <img class="card-img-top" width="286" height="180" src="{{ asset("images/$item->poster") }}" alt="Card image cap">
    <div class="card-body">
        @if ( $item->genre->nama==='film horor' )
        <span class="badge badge-danger ml-4">{{ $item->genre->nama }}</span>

        @else
        <span class="badge badge-info ml-4">{{ $item->genre->nama }}</span>

        @endif

      <h5 class="card-title mb-2 font-weight-bold">{{ $item->judul }}</h5>
      <span class="border-5 bg-red">{{ $item->tahun }}</span>
      <p class="card-text">{{ $item->ringkasan }}</p>
      <a href="/film/{{ $item->id }}" class="btn btn-primary">View</a>
    </div>
  </div>


    </div>
    @endforeach

</div>





@endsection
