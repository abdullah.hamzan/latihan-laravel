
@extends('layout.master')

@section('judul')

Update profile {{ $profile->name }}

@endsection

@section('content')

    <h2>Edit Profile</h2>
        <form action="/profile/{{ $profile->id }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" value="{{ $profile->umur }}"name="umur" id="umur" >
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea  class="form-control" id="bio" rows="10" name="bio">{{ $profile->bio }}</textarea>
                @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
              </div>
            <div class="form-group">
                <label for="umur">alamat</label>
                <input type="text" class="form-control" name="alamat" id="alamat" value="{{ $profile->alamat }}">
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>


            <button type="submit" class="btn btn-info">Update</button>
        </form>


@endsection
