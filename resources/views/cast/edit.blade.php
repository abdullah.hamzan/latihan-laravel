
@extends('layout.master')

@section('judul')

form cast edit {{ $cast->nama }}

@endsection

@section('content')

    <h2>Tambah Data</h2>
        <form action="/cast/{{ $cast->id }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama" value={{ $cast->nama }}>
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur" value={{ $cast->umur }}>
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea  class="form-control" id="bio" rows="10" name="bio" >{{ $cast->bio }}</textarea>
                @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
              </div>

            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>


@endsection
