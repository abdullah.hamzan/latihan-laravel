
@extends('layout.master')

@section('judul')
Data casts

@endsection

@section('content')

@auth
<a href="/cast/create" class="btn btn-success mb-3">Tambah Data</a>


@endauth


<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($casts as $key=>$item)

        <tr>
            <th scope="row">{{ $key+1 }}</th>
            <td>{{ $item->nama}}</td>
            <td>{{ $item->umur }}</td>
            <td>{{ $item->bio}}</td>
            <td>

                @auth
                <form action="/cast/{{ $item->id }}" method="post">
                    <a href="/cast/{{ $item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{ $item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @method('delete')
                    @csrf
                    <input type="submit" value="Hapus" class="btn btn-danger btn-sm ">

                </form>

                @endauth
                @guest
                <a href="/cast/{{ $item->id}}" class="btn btn-info btn-sm">Detail</a>
                @endguest








            </td>
          </tr>

        @empty
        <tr>
            <td>Data Masih Kosong</td>
        </tr>

        @endforelse


    </tbody>
  </table>

@endsection
