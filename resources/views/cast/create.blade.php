
@extends('layout.master')

@section('judul')

form cast

@endsection

@section('content')

    <h2>Tambah Data</h2>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea  class="form-control" id="bio" rows="10" name="bio"></textarea>
                @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
              </div>

            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>


@endsection
