
@extends('layout.master')
@section('judul')
Tabel Data

@endsection

@section('content')
<a href="/genre/create" class="btn btn-primary mb-2">Tambah</a>

<table class="table">

<thead class="thead-light">
<tr>
<th scope="col">#</th>
<th scope="col">Name</th>
<th scope="col" >Actions</th>
</tr>
</thead>
<tbody>
    @forelse ($genres as $key=>$item)

    <tr>
        <th scope="row">{{ $key+1 }}</th>
        <td>{{ $item->nama}}</td>
        <td>
            <form action="/genre/{{ $item->id }}" method="post">
                <a href="/genre/{{ $item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/genre/{{ $item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @method('delete')
                @csrf
                <input type="submit" value="Hapus" class="btn btn-danger btn-sm ">

            </form>

        </td>
      </tr>

    @empty
    <tr>
        <td>Data Masih Kosong</td>
    </tr>

    @endforelse



</tbody>

</table>

@endsection
