
@extends('layout.master')

@section('judul')

form Genre Film

@endsection

@section('content')

    <h2>Tambah Data Genre Film</h2>
        <form action="/genre" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama genre film">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>


@endsection
