
@extends('layout.master')

@section('judul')

form film

@endsection

@section('content')

    <h2>Update Data Genre Film</h2>
        <form action="/genre/{{ $genre->id }}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="nama">nama</label>
                <input type="text" class="form-control" name="nama" id="nama" value="{{ $genre->nama }}">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>


@endsection
