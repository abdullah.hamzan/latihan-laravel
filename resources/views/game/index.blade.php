
@extends('layout.master')
@section('judul')
Tabel Data

@endsection

@section('content')
<a href="/game/create" class="btn btn-primary mb-2">Tambah</a>

<table class="table">

<thead class="thead-light">
<tr>
<th scope="col">#</th>
<th scope="col">Name</th>
<th scope="col">Gameplay</th>
<th scope="col">Developer</th>
<th scope="col">Year</th>
<th scope="col" >Actions</th>
</tr>
</thead>
<tbody>
    @forelse ($games as $key=>$item)

    <tr>
        <th scope="row">{{ $key+1 }}</th>
        <td>{{ $item->name}}</td>
        <td>{{ $item->gameplay }}</td>
        <td>{{ $item->developer}}</td>
        <td>{{ $item->year}}</td>
        <td>


            <form action="/game/{{ $item->id }}" method="post">
                <a href="/game/{{ $item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/game/{{ $item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @method('delete')
                @csrf
                <input type="submit" value="Hapus" class="btn btn-danger btn-sm ">

            </form>





        </td>
      </tr>

    @empty
    <tr>
        <td>Data Masih Kosong</td>
    </tr>

    @endforelse



</tbody>

</table>

@endsection
