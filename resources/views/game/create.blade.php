
@extends('layout.master')

@section('judul')

Create Data Game

@endsection

@section('content')

    <h2>Tambah Data</h2>
        <form action="/game/store" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="bio">GamePlay</label>
                <textarea  class="form-control" id="bio" rows="10" name="gameplay"></textarea>
                @error('gameplay')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
              </div>

            <div class="form-group">
                <label for="developer">Developer</label>
                <input type="text" class="form-control" name="developer" id="developer" placeholder="Masukkan Developer">
                @error('developer')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="year">Year</label>
                <input type="text" class="form-control" name="year" id="year" placeholder="Masukkan Year">
                @error('year')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>


            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>


@endsection
