
@extends('layout.master')

@section('judul')

Edi data Game {{ $game->name }}

@endsection

@section('content')

    <h2>Tambah Data</h2>
        <form action="/game/{{ $game->id }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama" value="{{ $game->name }}">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="bio">GamePlay</label>
                <textarea  class="form-control" id="bio" rows="10" name="gameplay">{{ $game->gameplay }}</textarea>
                @error('gameplay')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
              </div>

            <div class="form-group">
                <label for="developer">Developer</label>
                <input type="text" class="form-control" name="developer" value="{{ $game->developer }}" id="developer" placeholder="Masukkan Developer">
                @error('developer')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="year">Year</label>
                <input type="text" class="form-control" name="year" value="{{ $game->year }}" id="year" placeholder="Masukkan Year">
                @error('year')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>


            <button type="submit" class="btn btn-primary">Update</button>
        </form>


@endsection
