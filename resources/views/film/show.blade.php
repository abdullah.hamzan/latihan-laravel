
@extends('layout.master')

@section('judul')

Detail Films

@endsection

@section('content')
<div class="card mb-3">
    <img class="card-img-top" width="1149" height="700" src="{{ asset("images/$film->poster") }}" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title font-weight-bold">{{ $film->judul }}</h5><br>
      <h6 class="card-title font-weight-bold mt-2 mb-2">{{ $film->tahun }}</h6>
      <p class="card-text">{{ $film->ringkasan }}</p>
      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
    </div>
  </div>

@endsection
