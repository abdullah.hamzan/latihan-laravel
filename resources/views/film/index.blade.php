
@extends('layout.master')

@section('judul')
Data films

@endsection

@section('content')

<a href="/film/create" class="btn btn-success mb-3">Tambah Data</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Judul</th>
        <th scope="col">Ringkasan</th>
        <th scope="col">Tahun</th>
        <th scope="col">Poster</th>

        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($films as $key=>$item)

        <tr>
            <th scope="row">{{ $key+1 }}</th>
            <td>{{ $item->judul}}</td>
            <td>{{ $item->ringkasan }}</td>
            <td>{{ $item->tahun}}</td>
            <td><img height="80" width="80" src={{ asset("images/$item->poster")}}  alt="" ></td>

            <td>

                <form action="/film/{{ $item->id }}" method="post">
                    <a href="/film/{{ $item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/film/{{ $item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @method('delete')
                    @csrf
                    <input type="submit" value="Hapus" class="btn btn-danger btn-sm ">

                </form>





            </td>
          </tr>

        @empty
        <tr>
            <td>Data Masih Kosong</td>
        </tr>

        @endforelse


    </tbody>
  </table>

@endsection
