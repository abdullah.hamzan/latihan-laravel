
@extends('layout.master')

@section('judul')

Form Update film

@endsection

@section('content')

    <h2>Udate Data</h2>
        <form action="/film/{{ $film->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" name="judul" id="ringkasan" value="{{ $film->judul }}">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="ringkasan">Ringkasan</label>
                <textarea  class="form-control" id="ringkasan" rows="10" name="ringkasan">{{ $film->ringkasan }}</textarea>
                @error('ringkasan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
              </div>

            <div class="form-group">
                <label for="tahun">Tahun</label>
                <input type="text" class="form-control" name="tahun" id="tahun" value="{{ $film->tahun }}">
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="poster">poster</label>
                <input type="file" class="form-control" name="poster" id="poster" placeholder="Masukkan Gambar">
                @error('poster')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Options</label>
                </div>
                <select class="custom-select" id="inputGroupSelect01" name="genre_id">
                  <option selected >Choose genre film</option>
                  @foreach ($genre as $item)
                  @if ($item->id===$film->genre_id)

                  <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>

                  @else
                  <option value="{{ $item->id }}">{{ $item->nama }}</option>

                  @endif


                  @endforeach

                </select>
                @error('genre_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
              </div>



            <button type="submit" class="btn btn-warning">Update</button>
        </form>


@endsection
