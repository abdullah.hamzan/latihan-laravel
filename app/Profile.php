<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //

    protected $fillable=[
        'umur','bio','alamat'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
