<?php

namespace App\Http\Controllers;

use App\Cast;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;





class CastController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);

    }
    //
    public function create(){
        return view('cast.create');

    }
    public function store(Request $request){

        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        // query builder

        // DB::table('casts')->insert([
        //     'nama' => $request['nama'],
        //     'umur' => $request['umur'],
        //     'bio' => $request['bio'],
        // ]);

        // ORM
        Cast::create([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],

        ]);

        return redirect('cast');

    }

    public function index(){


        // Query Builder
        // $casts = DB::table('casts')->get();

        // ORM
        $casts = Cast::all();

        return view('cast.index',compact('casts'));

    }

    public function show ($id){

        // Query Builder
        // $cast= DB::table('casts')->where('id', $id)->first();

        // ORM
         $cast =Cast::find($id);

        return view('cast.show',compact('cast'));

    }

    public function edit ($id){


        // query builder
        // $cast= DB::table('casts')->where('id', $id)->first();

        // ORM
        $cast=Cast::find($id);

        return view('cast.edit',compact('cast'));

    }

    public function update ($id, Request $request){

        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        // query builder
        // $affected = DB::table('casts')
        //       ->where('id', $id)
        //       ->update([

        //         'nama' => $request['nama'],
        //         'umur' => $request['umur'],
        //         'bio' => $request['bio'],

        //     ]);

        // ORM

        $cast=Cast::find($id);
        $cast->nama=$request['nama'];
        $cast->umur=$request['umur'];
        $cast->bio=$request['bio'];
        $cast->update();

         return redirect('cast');


    }

    public function destroy($id){

         // Query builder
        // DB::table('casts')->where('id', $id)->delete();

        // ORM
        $cast=Cast::find($id);
        $cast->delete();

        return redirect('cast');

    }
}
