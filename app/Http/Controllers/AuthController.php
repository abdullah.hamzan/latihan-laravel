<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //

    public function register(){

        return view('halaman.form');

    }

    public function welcome(Request $request){
        // dd($request->all());
        $firstName= $request->firstName;
        $lastName= $request->lastName;
        return view('halaman.welcome1',compact('firstName','lastName'));

    }
}
