<?php

namespace App\Http\Controllers;

use App\Film;
use Illuminate\Http\Request;


class IndexController extends Controller
{

    public function home(){

        $films = Film::all();

        return view('halaman.index',compact('films'));

    }
}
