<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;



class ProfileController extends Controller
{
    //

    public function index(){

        $profile=Profile::where('user_id',Auth::id())->first();


        return view('profile.index',compact('profile'));

    }
    public function update ($id, Request $request){

        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
        ]);


        $profile=Profile::find($id);
        $profile->umur=$request['umur'];
        $profile->bio=$request['bio'];
        $profile->alamat=$request['alamat'];
        $profile->update();

         return redirect('profile');


    }

}
