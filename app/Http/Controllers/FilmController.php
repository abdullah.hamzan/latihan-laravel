<?php

namespace App\Http\Controllers;

use App\Film;
use App\Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $films = Film::all();

        return view('film.index',compact('films'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $genre=Genre::all();
        return view('film.create',compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $posterName = time() . '.' . $request->poster->extension();
        $request->poster->move(public_path('images'), $posterName);

        Film::create([
            'judul'=>$request['judul'],
            'ringkasan'=>$request['ringkasan'],
            'tahun'=>$request['tahun'],
            'genre_id'=>$request['genre_id'],
            'poster'=>$posterName
        ]);
        // $film = new Film;

        // $film->judul = $request->judul;
        // $film->ringkasan = $request->ringaksan;
        // $film->tahun = $request->tahun;
        // $film->poster = $posterName;


        // $film->save();

        return redirect('film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $film =Film::find($id);

        return view('film.show',compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $film=Film::find($id);
        $genre=Genre::all();


        return view('film.edit',compact('film','genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id'=>'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);


        if ($request->has('poster')){

                $film=Film::find($id);
                $path ="images/";

                File::delete($path . $film->poster);

                $film->judul=$request['judul'];
                $film->ringkasan=$request['ringkasan'];
                $film->tahun=$request['tahun'];
                $film->genre_id=$request['genre_id'];
                $posterName = time() . '.' . $request->poster->extension();
                $request->poster->move(public_path('images'), $posterName);

                $film->poster=$posterName;
                $film->update();


                 return redirect('film');


        }else {
              $film=Film::find($id);
                $film->judul=$request['judul'];
                $film->ringkasan=$request['ringkasan'];
                $film->tahun=$request['tahun'];
                $film->genre_id=$request['genre_id'];
                $posterName = time() . '.' . $request->poster->extension();
                $request->poster->move(public_path('images'), $posterName);

                $film->poster=$posterName;
                $film->update();
                
                return redirect('film');
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $film=Film::find($id);
        $film->delete();

        return redirect('film');
    }
}
