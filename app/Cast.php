<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    //

    protected $table ="casts";
    protected $fillable=[
        "nama","umur","bio","created_at","updated_at"
    ];

}
