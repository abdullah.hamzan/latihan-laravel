<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    //
    protected $table='films';
    protected $fillable=[
        'judul','ringkasan','tahun','poster','genre_id','created_at','updated_at'
    ];
    public function genre(){

        return $this->belongsTo('App\Genre');
    }

}
