<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

Route::get('/', 'IndexController@home');

Route::get('/master', function () {
    return view('layout.master');
});


Route::get('/data-tables',function(){
    return view('table.datatable');
});
Route::get('/table',function(){
    return view('table.table');
});

// CRUD cast
// Route::get('/cast','CastController@index');
// Route::get('/cast/create','CastController@create');
// Route::post('/cast/store','CastController@store');
// Route::get('/cast/{cast_id}','CastController@show');
// Route::get('/cast/{cast_id}/edit','CastController@edit');
// Route::put('/cast/{cast_id}','CastController@update');
// Route::delete('/cast/{cast_id}','CastController@destroy');




Route::middleware(['auth'])->group(function () {

    //CRUD casts
// Route::resource('/cast','CastController');
 Route::resource('/profile','ProfileController')->only([
     'index','update'
 ]);




});


Route::middleware(['auth'])->group(function () {

    //CRUD casts
 Route::resource('/genre','GenreController');
 //CRUD film
 Route::resource('/film','FilmController');


});

Route::resource('/cast','CastController');




// CRUD Game
Route::get('/game','GameController@index');
Route::get('/game/create','GameController@create');
Route::post('/game/store','GameController@store');
Route::get('/game/{game_id}','GameController@show');
Route::get('/game/{game_id}/edit','GameController@edit');
Route::put('/game/{game_id}','GameController@update');
Route::delete('/game/{game_id}','GameController@destroy');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



